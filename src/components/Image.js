import React from 'react'

const Image = ({ url }) => {
  return (
    <div className="col-xs-12 col-sm-6 col-md-4 col-lg-4  image">
      <img src={url} alt="burger"></img>
    </div>
  )
}

export default Image
