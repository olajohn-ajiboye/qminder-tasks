import React from 'react'
import Image from './Image'

export default function ImageList({ photosUrl }) {
  return (
    <div className="container row img-list">
      {photosUrl.map((url, index) => {
        return <Image key={index} url={url}></Image>
      })}
    </div>
  )
}
