import React, { Component } from 'react'
import GoogleMapReact from 'google-map-react';


const Map = ({ name }) => <div>{name}</div>;
class MapWrapper extends Component {

  static defaultProps = {
    center: { lat: 10, lng: -35 },
    zoom: 5
  };

  render() {
    return (
      <div style={{ height: '50vh', width: '70%' }}>
        Api key is not activated, will never work
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyD-jZIenwFra1h47AGF4w7krk6O0RTYVEw' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}

        >
          {this.props.locations.map((location, index) => {
            const { name, lat, lng } = location
            return <Map
              lat={lat}
              lng={lng}
              text={name}
              key={index}
            />
          })}
        </GoogleMapReact>
      </div>
    )
  }
}
export { MapWrapper }
