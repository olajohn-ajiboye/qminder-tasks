import React, { Component } from 'react'
import axios from 'axios'
import ImageList from './components/ImageList'
import Center from 'react-center';
import { baseUrl, clientID, clientSecret, v, venueUrl, photos } from './constants'
import { MapWrapper } from './components/MapWrapper'
import './App.css'
export default class App extends Component {
  state = {
    venues: [],
    city: "tartu",
    query: "burger",
    photosUrl: photos, /*Local data stored from APi response from Querying Data for "New York and Pizza"
    Foursquare API has very limited calls allowed per day */
  }
  getVenuesData = async () => {
    try {
      const { city, query } = this.state
      const { data } = await axios.get(`${venueUrl}&intent=browse&radius=10000&near=${city}&query=${query}`);
      this.setState({ venues: data.response.venues })
      return data.response.venues;
    } catch (err) {
      console.log(err);
    }
  };

  getPhotosUrl = async () => {
    try {
      const venues = await this.getVenuesData();
      const venueIDs = venues.map(venue => venue.id);
      const promises = venueIDs.map(async id => {
        const apiResponse = await axios.get(
          `${baseUrl}/${id}/photos?client_id=${clientID}&client_secret=${clientSecret}&v=${v}&group=venue&limit=100`
        );
        const { items } = await apiResponse.data.response.photos;
        const formattedUrl = items.map(d => {
          const photoUrl = `${d.prefix}300x500${d.suffix}`;
          return photoUrl;
        });
        return formattedUrl;
      });
      const photosUrl = await Promise.all(promises);
      this.setState({ photosUrl, loading: false });
    } catch (err) {
      console.error('Error: ', err);
    }
  };
  componentDidMount() {

    // FOURSQUARE has a very limited Api call, uncomment the function below to get live request
    // I have somulated a live request by storing sample response in constants/index.js
    //this.getPhotosUrl();
    this.getVenuesData()

  }

  render() {
    const { photosUrl, venues } = this.state
    // formatted data used for fetching Google Maps
    const locations = venues.map(v => [v.name, v.location.lat, v.location.lng])

    return (
      <>
        <div className="mt-5">
          <Center>
            <MapWrapper locations={locations}></MapWrapper>
          </Center>
        </div>

        <Center>
          <div className=" app">
            <ImageList photosUrl={photosUrl} />
          </div>
        </Center>
      </>
    )
  }
}

