
const clientID = `QMQC2VD1R2ILYZSIGQFUSLHPL2O13K23ZEZBEB4XDIJGESM3`;
const clientSecret = `NZVULN3WHFY1EX4CPK04T0DBJCJKPCNKFUM2NC0JSBJDDCXB`;
const baseUrl = `https://api.foursquare.com/v2/venues`;
const v = `20190615`;
const venueUrl = `${baseUrl}/search?client_id=${clientID}&client_secret=${clientSecret}&v=${v}`;
const photos = [
  ["https://fastly.4sqi.net/img/general/300x500/23849872_5hkqAsdGmX4aN_PD74S-gCP4n0NMpI0bwe0Oy7AuLV0.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/166408845_RMqvugH2slrIkzmUIr_vVod7r4SIx5AjvcnT6CvGtos.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/796716_OXm-MM-zWV0mQO7_N7vhTl4o6UeL-mPYurCMEOYxh8c.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/2448608_wGMQKZpsgEYak8ds4nGcAATNs53HXxHOmsohd9Fdf9E.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/21387947_fCjLOPXQlsIWlwngQviezH4pBuX3dFxyxp68YcorRBw.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/197570012_9LRVIGaEsGS8RAU5et--AuoLGfXcAcUxFZPUZtJ-rao.jpg"], ["https://fastly.4sqi.net/img/general/300x500/547742132_iW_G7aP7VqQ5ag6bEMUj2uEiuMX7FoU_8dPL3cOS1J8.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/127163888_aYFbGSr5xyBKhTwdGs0sR68ZwWI1IVf5jHaF4-_Lfc4.jpg"],
  ["https://fastly.4sqi.net/img/general/300x500/3192530_R3dtWhoBE9KaCVUjEaT1UwIknPTt49NEqZ-I6Nbo4G0.jpg"]
]

export { baseUrl, clientID, clientSecret, v, venueUrl, photos }

// sample response from Quering NewYour for Piza, A part of it stored in the photos constant

// 12: Array["https://fastly.4sqi.net/img/general/300x500/23670263_m5-usdEa751CL4yQxZeVQnHtumsRw6uxjngfPg3vjVU.jpg"]

// 13: Array["https://fastly.4sqi.net/img/general/300x500/12300730_Dk9ZN7G78MWp0Su8M5fqqT_Jg4ZpTN5Qj7rdLGq1hV8.jpg"]

// 14: Array["https://fastly.4sqi.net/img/general/300x500/19362285_Q-Ee2HuneR4b5_MWTmV-e9QllAUC1Kwq10e6hm9X3lY.jpg"]

// 15: Array["https://fastly.4sqi.net/img/general/300x500/151543493_zWICXImCZr9XeC6_igWl9UJ3IzkhCwXHgAWRegRawNs.jpg"]

// 16: Array["https://fastly.4sqi.net/img/general/300x500/530501054_-hmLaXbPUTphJQrDdAzC5Ylj8Erf-Zooi_R0488ugdw.jpg"]

// 17: Array["https://fastly.4sqi.net/img/general/300x500/7328246__HNfO3hF_C234B9_SrNscv9irKcThNVdW2VGo6RsP6k.jpg"]

// 18: Array["https://fastly.4sqi.net/img/general/300x500/2006147_RgNFibxgE2FCPdx0sDJUDCE9SQ2EbZhfnGM5HfY4OAA.jpg"]

// 19: Array["https://fastly.4sqi.net/img/general/300x500/1052681_cNNwp1Nj4ZOKBOWQERG0zwpZpChJ6iyAk69t1wnmhC0.jpg"]

// 20: Array["https://fastly.4sqi.net/img/general/300x500/12011463_mjUzQw2EXd4W77P9I7iE9Jm9nfKPApj67zngNDKxCpE.jpg"]

// 21: Array["https://fastly.4sqi.net/img/general/300x500/30271237_A5LRukInYiiu58C9G9NhIkvepK60FHpT60rkEGyabu0.jpg"]

// 22: Array["https://fastly.4sqi.net/img/general/300x500/54934764_cXvbWUZs8JYg2ml6VDZJDiUDE4wLAD2Fd6hIMT6nBeY.jpg"]

// 23: Array["https://fastly.4sqi.net/img/general/300x500/23670263_suG2oS5YXZK8uPHy763jqQrB6gdyvGZ7kGdvAI1uIP4.jpg"]

// 24: Array["https://fastly.4sqi.net/img/general/300x500/127545944_U-YT2_PCoaRs8OLTxX4jv5fOoWncsRtYMGEzOCIm3c0.jpg"]

// 25: Array["https://fastly.4sqi.net/img/general/300x500/81606802_ZqFtSM679JAVbCbooKXiunPDzDe_7-QtcPEMfXBU9nk.jpg"]

// 26: Array["https://fastly.4sqi.net/img/general/300x500/13194532_NcplYydLSc2UJ6h5TkeFreZh0BgzqwCRJPhMYwLgdnc.jpg"]

// 27: Array["https://fastly.4sqi.net/img/general/300x500/19173375_ChjU_o5cWGPtvYJJMLEYqjYEhlHB2n80muQT4N8E7Og.jpg"]

// 28: Array["https://fastly.4sqi.net/img/general/300x500/30311518_9k46s8h_WLHrF3jO1RlLwemP2P2CU5qyL_aG29mso6Y.jpg"]

// 29: Array["https://fastly.4sqi.net/img/general/300x500/88309421_HD1SYojjSvdI51qMEL1c1dhlE7ye5a4IsscfbDRG484.jpg"
